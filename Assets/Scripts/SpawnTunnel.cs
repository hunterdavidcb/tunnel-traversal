﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTunnel : MonoBehaviour
{
	List<GameObject> tunnels = new List<GameObject>();

	public GameObject tunnelPrefab;
	float tunnelSize = 4f;
    // Start is called before the first frame update
    void Start()
    {
		Next();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	void Next()
	{
			GameObject tunnel = Instantiate(tunnelPrefab);
			
			
			Vector3 pos;
			Vector3 backScale;
			if (tunnels.Count > 0)
			{
				GameObject prev = tunnels[tunnels.Count - 1];
				pos = prev.transform.position;
				pos.z += tunnelSize;
				//we do this because the local rotation of the front and back bones is different
				Vector3 swap = prev.GetComponent<AdjustSize>().frontBone.localScale;
				backScale = new Vector3(swap.x,swap.z,swap.y);
			}
			else
			{
				pos = Vector3.zero;
				pos.z += tunnelSize;
				float scale = Random.Range(1f, 4f);
				backScale = new Vector3(scale, scale, 1f);
			}

			tunnel.transform.position = pos;
			tunnel.GetComponent<AdjustSize>().rearBone.localScale = backScale;
			tunnel.GetComponent<AdjustSize>().Entered += OnEntered;

			float fScale = Random.Range(1f, 4f);
			Vector3 frontScale = new Vector3(fScale, 1f, fScale);
			tunnel.GetComponent<AdjustSize>().frontBone.localScale = frontScale;

			tunnels.Add(tunnel);

			if (tunnels.Count > 4)
			{
				GameObject rem = tunnels[0];
				tunnels.Remove(rem);
				Destroy(rem);
			}

			Mesh m = new Mesh();
			tunnel.GetComponentInChildren<SkinnedMeshRenderer>().BakeMesh(m);
			//for (int i = 0; i < m.vertexCount; i++)
			//{
			//	m.vertices[i] = new Vector3(m.vertices[i].x / 100f, m.vertices[i].y / 100f, m.vertices[i].z / 100f);
			//}
			//m.RecalculateBounds();
			tunnel.GetComponentInChildren<SkinnedMeshRenderer>().gameObject.AddComponent<MeshCollider>().sharedMesh = m;
	}

	private void OnEntered()
	{
		Next();
	}
}
