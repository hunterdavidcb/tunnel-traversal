﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
	float speed = 2f;

	bool running = true;

	// Start is called before the first frame update
	void Start()
    {
		StartCoroutine(StartMove());
		GetComponent<AdjustScale>().Moving += OnMovementStarted;
		GetComponent<AdjustScale>().Stopped += OnMovementStopped;
	}

	private void OnMovementStopped()
	{
		Debug.Log("stopping");
		running = false;
		StopAllCoroutines();
	}

	private void OnMovementStarted()
	{
		if (!running)
		{
			Debug.Log("starting");
			StartCoroutine(StartMove());
		}
		
	}

	// Update is called once per frame
	void Update()
    {
		
    }

	IEnumerator StartMove()
	{
		running = true;
		while (true)
		{
			transform.Translate(transform.forward * speed * Time.deltaTime);
			yield return null;
		}
		
	}
}
