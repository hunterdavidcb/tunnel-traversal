﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustScale : MonoBehaviour
{
	public Transform left, right, bottom;
	public Transform leftTarget, rightTarget;
	public ParticleSystem leftPS, rightPS, bottomPS;
	public LineRenderer leftLR, rightLR, bottomLR;

	float score = 0f;

	public delegate void ScoreHandler(float s, float d);
	public event ScoreHandler ScoreChanged;

	public delegate void MovementHandler();
	public event MovementHandler Moving;
	public event MovementHandler Stopped;

	Gradient g = new Gradient();
	Gradient r = new Gradient();
	bool collidingLeft = false;
	bool collidingRight = false;
	bool collidingBottom = false;
	bool moving = true;

	// Start is called before the first frame update
	void Start()
    {
		g.SetKeys(new GradientColorKey[2]
			{ new GradientColorKey(Color.green, 0f), new GradientColorKey(Color.green, 1f) },
			new GradientAlphaKey[2] { new GradientAlphaKey(1f, 0f), new GradientAlphaKey(1f, 1f) });
		r.SetKeys(new GradientColorKey[2]
			{ new GradientColorKey(Color.red, 0f), new GradientColorKey(Color.red, 1f) },
			new GradientAlphaKey[2] { new GradientAlphaKey(1f, 0f), new GradientAlphaKey(1f, 1f) });

		left.GetComponent<ColliderHolder>().CollisionEntered += OnCollisionStart;
		left.GetComponent<ColliderHolder>().CollisionExited += OnCollisionEnd;

		right.GetComponent<ColliderHolder>().CollisionEntered += OnCollisionStart;
		right.GetComponent<ColliderHolder>().CollisionExited += OnCollisionEnd;

		bottom.GetComponent<ColliderHolder>().CollisionEntered += OnCollisionStart;
		bottom.GetComponent<ColliderHolder>().CollisionExited += OnCollisionEnd;
	}

	private void OnCollisionEnd(int i)
	{
		int total = 0;

		switch (i)
		{
			case 1:
				collidingLeft = false;
				break;
			case 2:
				collidingRight = false;
				break;
			case 3:
				collidingBottom = false;
				break;
		}

		if (collidingLeft)
		{
			total += 1;
		}

		if (collidingRight)
		{
			total += 1;
		}

		if (collidingBottom)
		{
			total += 1;
		}

		if (total >= 2)
		{
			moving = false;
			if (Stopped != null)
			{
				Stopped();
			}
		}
		else
		{
			moving = true;
			if (Moving != null)
			{
				Moving();
			}
		}
	}

	private void OnCollisionStart(int i)
	{
		Debug.Log("hey");
		int total = 0;
		switch (i)
		{
			case 1:
				collidingLeft = true;
				TriggerPS(leftPS);
				break;
			case 2:
				collidingRight = true;
				TriggerPS(rightPS);
				break;
			case 3:
				collidingBottom = true;
				TriggerPS(bottomPS);
				break;
		}

		if (collidingLeft)
		{
			total += 1;
		}

		if (collidingRight)
		{
			total += 1;
		}

		if (collidingBottom)
		{
			total += 1;
		}

		if (total >= 2)
		{
			moving = false;
			if (Stopped != null)
			{
				Stopped();
			}
		}
		else
		{
			moving = true;
			if (Moving != null)
			{
				Moving();
			}
		}
	}

	// Update is called once per frame
	void Update()
    {
		if (Input.GetMouseButton(0) && !collidingLeft && !collidingRight && !collidingBottom)
		{
			transform.localScale = new Vector3(Mathf.Clamp(transform.localScale.x + Time.deltaTime, 1f, 4f),
				Mathf.Clamp(transform.localScale.y + Time.deltaTime, 1f, 4f),
				Mathf.Clamp(transform.localScale.z + Time.deltaTime, 1f, 4f));
		}

		if (Input.GetMouseButton(1))
		{
			transform.localScale = new Vector3(Mathf.Clamp(transform.localScale.x - Time.deltaTime, 1f, 4f),
				Mathf.Clamp(transform.localScale.y - Time.deltaTime, 1f, 4f),
				Mathf.Clamp(transform.localScale.z - Time.deltaTime, 1f, 4f));
		}

		//Debug.DrawRay(left.position, leftTarget.position - left.position);
		//Debug.DrawRay(right.position, rightTarget.position - right.position);
		//Debug.DrawRay(bottom.position, Vector3.down);
		TestDistance();
	}

	void TestDistance()
	{
		RaycastHit hit;
		float dl = 0f;
		float dr = 0f;
		float db = 0f;

		if (Physics.Raycast(left.position, leftTarget.position - left.position, out hit))
		{

			leftLR.SetPosition(0, left.position);
			leftLR.SetPosition(1, hit.point);
			dl = Vector3.Distance(left.position, hit.point);
			SetGradient(leftLR, dl, 1);
		}
		else
		{
			leftLR.SetPosition(0, left.position);
			leftLR.SetPosition(1, left.position);
		}

		if (Physics.Raycast(right.position, rightTarget.position - right.position, out hit))
		{
			//Debug.DrawLine(right.position, hit.point);
			rightLR.SetPosition(0, right.position);
			rightLR.SetPosition(1, hit.point);
			dr = Vector3.Distance(right.position, hit.point);
			SetGradient(rightLR, dr, 2);
		}
		else
		{
			rightLR.SetPosition(0, right.position);
			rightLR.SetPosition(1, right.position);
		}

		if (Physics.Raycast(bottom.position, Vector3.down, out hit))
		{
			//Debug.DrawLine(bottom.position, hit.point);
			bottomLR.SetPosition(0, bottom.position);
			bottomLR.SetPosition(1, hit.point);
			db = Vector3.Distance(bottom.position, hit.point);
			SetGradient(bottomLR, db, 3);
		}
		else
		{
			bottomLR.SetPosition(0, bottom.position);
			bottomLR.SetPosition(1, bottom.position);
		}

		score += CalculateScore(dl, dr, db);

		
		if (ScoreChanged != null)
		{
			ScoreChanged(score, (dl + dr + db)/3f);
		}

	}

	void SetGradient(LineRenderer lr, float d, int i)
	{
		switch (i)
		{
			case 1:
				if (d < 0.1f || d > 1.5f || collidingLeft)
				{
					lr.colorGradient = r;
				}
				else
				{
					lr.colorGradient = g;
				}
				break;
			case 2:
				if (d < 0.1f || d > 1.5f || collidingRight)
				{
					lr.colorGradient = r;
				}
				else
				{
					lr.colorGradient = g;
				}
				break;
			case 3:
				if (d < 0.1f || d > 1.5f || collidingBottom)
				{
					lr.colorGradient = r;
				}
				else
				{
					lr.colorGradient = g;
				}
				break;
			default:
				break;
		}
		
	}

	float CalculateScore(float dl, float dr, float db)
	{
		if (!moving)
		{
			return 0f;
		}

		float d = (dl + dr + db) / 3f;

		//we have a collision somewhere so the score should be negative
		if (collidingLeft || collidingRight || collidingBottom)
		{
			return -(d * d / 4 + 4) * Time.deltaTime;
		}
		else
		{
			return (d * d / 4 + 4) * Time.deltaTime;
		}

		//if (d < 0.1f || d > 1.5f )//|| colliding)
		//{
			
		//	switch (i)
		//	{
		//		case 1:
		//			leftLR.colorGradient = r;
		//			break;
		//		case 2:
		//			rightLR.colorGradient = r;
		//			break;
		//		case 3:
		//			bottomLR.colorGradient = r;
		//			break;
		//	}
		//	return -(d * d / 4 + 4) * Time.deltaTime;
		//}
		//else
		//{
		//	switch (i)
		//	{
		//		case 1:
		//			leftLR.colorGradient = g;
		//			break;
		//		case 2:
		//			rightLR.colorGradient = g;
		//			break;
		//		case 3:
		//			bottomLR.colorGradient = g;
		//			break;
		//	}
		//	return (d * d / 4 + 4) * Time.deltaTime;
		//}
	}

	void TriggerPS(ParticleSystem ps)
	{
		ps.Play();
	}

	//private void OnCollisionEnter(Collision collision)
	//{
	//	colliding = true;
	//	//Debug.Log("hit");
	//}

	//private void OnCollisionExit(Collision collision)
	//{
	//	colliding = false;
	//	//Debug.Log("left");
	//}
}
