﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustSize : MonoBehaviour
{
	public Transform rearBone;
	public Transform frontBone;
	bool sent = false;
	public delegate void EnterHandler();
	public event EnterHandler Entered;

	private void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player" && !sent)
		{
			sent = true;
			if (Entered != null)
			{
				Entered();
			}
			//Debug.Log("player");
		}
	}
}
