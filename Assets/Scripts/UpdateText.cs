﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateText : MonoBehaviour
{
	Text text;
    // Start is called before the first frame update
    void Start()
    {
		text = GetComponent<Text>();
		FindObjectOfType<AdjustScale>().ScoreChanged += OnScoreChanged;
    }

	private void OnScoreChanged(float s, float d)
	{
		text.text = "Score: " + s.ToString("0.00") + "\nDistance to Walls: " + d.ToString("0.00");
	}

	// Update is called once per frame
	void Update()
    {
        
    }
}
