﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderHolder : MonoBehaviour
{
	public delegate void CollisionHandler(int i);
	public event CollisionHandler CollisionEntered;
	public event CollisionHandler CollisionExited;
	public event CollisionHandler CollisionStayed;

	public int pos;

	public AudioClip[] screech;

	AudioSource source;

	private void Start()
	{
		source = GetComponent<AudioSource>();
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (!source.isPlaying)
		{
			source.clip = screech[Random.Range(0, screech.Length)];
			source.Play();
		}
		
		if (CollisionEntered != null)
		{
			CollisionEntered(pos);
		}
	}

	private void OnCollisionStay(Collision collision)
	{
		if (CollisionStayed != null)
		{
			CollisionStayed(pos);
		}
	}

	private void OnCollisionExit(Collision collision)
	{
		source.Stop();
		if (CollisionExited != null)
		{
			CollisionExited(pos);
		}
	}
}
