﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
	public Transform player;
	Vector3 v;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		Vector3 t = player.position;
		t.z -= 4f;
		t.y = 0.5f;
		transform.position = Vector3.SmoothDamp(transform.position, t, ref v, 0.5f);
    }
}
