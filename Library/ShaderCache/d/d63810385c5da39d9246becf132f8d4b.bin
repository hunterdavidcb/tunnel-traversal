<Q                         _ADDITIONAL_LIGHTS     _MAIN_LIGHT_SHADOWS    _MIXED_LIGHTING_SUBTRACTIVE     lA  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_LightData;
	UNITY_UNIFORM vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM vec4 unity_SHAr;
	UNITY_UNIFORM vec4 unity_SHAg;
	UNITY_UNIFORM vec4 unity_SHAb;
	UNITY_UNIFORM vec4 unity_SHBr;
	UNITY_UNIFORM vec4 unity_SHBg;
	UNITY_UNIFORM vec4 unity_SHBb;
	UNITY_UNIFORM vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec3 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TANGENT0;
in highp vec4 in_TEXCOORD0;
out highp vec3 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
out highp vec3 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD8;
out highp vec4 vs_TEXCOORD9;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
vec3 u_xlat3;
float u_xlat12;
void main()
{
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat1 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD0.xyz = u_xlat0.xyz;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = max(u_xlat12, 1.17549435e-38);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat1.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat12 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat12 = inversesqrt(u_xlat12);
    vs_TEXCOORD1.xyz = vec3(u_xlat12) * u_xlat1.xyz;
    u_xlat2.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_TANGENT0.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_TANGENT0.zzz + u_xlat2.xyz;
    u_xlat12 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat12 = max(u_xlat12, 1.17549435e-38);
    u_xlat12 = inversesqrt(u_xlat12);
    u_xlat2.xyz = vec3(u_xlat12) * u_xlat2.xyz;
    u_xlat2.w = in_TANGENT0.w;
    u_xlat12 = dot(u_xlat2, u_xlat2);
    u_xlat12 = inversesqrt(u_xlat12);
    vs_TEXCOORD2 = vec4(u_xlat12) * u_xlat2;
    vs_TEXCOORD3 = in_TEXCOORD0;
    vs_TEXCOORD4.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat3.xyz = u_xlat1.zxy * u_xlat2.yzx;
    u_xlat2.xyz = u_xlat1.yzx * u_xlat2.zxy + (-u_xlat3.xyz);
    vs_TEXCOORD5.xyz = u_xlat2.xyz * in_TANGENT0.www;
    u_xlat12 = u_xlat1.y * u_xlat1.y;
    u_xlat12 = u_xlat1.x * u_xlat1.x + (-u_xlat12);
    u_xlat2 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat3.x = dot(unity_SHBr, u_xlat2);
    u_xlat3.y = dot(unity_SHBg, u_xlat2);
    u_xlat3.z = dot(unity_SHBb, u_xlat2);
    u_xlat2.xyz = unity_SHC.xyz * vec3(u_xlat12) + u_xlat3.xyz;
    u_xlat1.w = 1.0;
    u_xlat3.x = dot(unity_SHAr, u_xlat1);
    u_xlat3.y = dot(unity_SHAg, u_xlat1);
    u_xlat3.z = dot(unity_SHAb, u_xlat1);
    u_xlat1.xyz = u_xlat2.xyz + u_xlat3.xyz;
    vs_TEXCOORD7.xyz = max(u_xlat1.xyz, vec3(0.0, 0.0, 0.0));
    vs_TEXCOORD8 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4_MainLightWorldToShadow[1];
    u_xlat1 = hlslcc_mtx4x4_MainLightWorldToShadow[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat0 = hlslcc_mtx4x4_MainLightWorldToShadow[2] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD9 = u_xlat0 + hlslcc_mtx4x4_MainLightWorldToShadow[3];
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0_0_0[4];
uniform 	vec4 _MainLightPosition;
uniform 	vec4 _MainLightColor;
uniform 	vec4 _AdditionalLightsCount;
uniform 	vec4 _AdditionalLightsPosition[32];
uniform 	vec4 _AdditionalLightsColor[32];
uniform 	vec4 _AdditionalLightsAttenuation[32];
uniform 	vec4 _AdditionalLightsSpotDir[32];
uniform 	vec4 _AdditionalLightsOcclusionProbes[32];
uniform 	vec4 _TimeParameters;
uniform 	vec4 _MainLightShadowParams;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM vec4 unity_WorldTransformParams;
	UNITY_UNIFORM vec4 unity_LightData;
	UNITY_UNIFORM vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM vec4 unity_SHAr;
	UNITY_UNIFORM vec4 unity_SHAg;
	UNITY_UNIFORM vec4 unity_SHAb;
	UNITY_UNIFORM vec4 unity_SHBr;
	UNITY_UNIFORM vec4 unity_SHBg;
	UNITY_UNIFORM vec4 unity_SHBb;
	UNITY_UNIFORM vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 Color_6DC7EDD5;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(1) uniform mediump sampler2DShadow hlslcc_zcmp_MainLightShadowmapTexture;
uniform mediump sampler2D _MainLightShadowmapTexture;
in highp vec3 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec4 vs_TEXCOORD3;
in highp vec3 vs_TEXCOORD4;
in highp vec3 vs_TEXCOORD7;
in highp vec4 vs_TEXCOORD9;
layout(location = 0) out highp vec4 SV_TARGET0;
vec4 u_xlat0;
vec3 u_xlat1;
vec2 u_xlat2;
vec4 u_xlat3;
mediump float u_xlat16_3;
vec3 u_xlat4;
vec3 u_xlat5;
mediump vec4 u_xlat16_5;
vec3 u_xlat6;
vec3 u_xlat7;
vec3 u_xlat10;
vec3 u_xlat11;
mediump float u_xlat16_11;
bool u_xlatb11;
vec2 u_xlat17;
vec2 u_xlat18;
bool u_xlatb18;
int u_xlati19;
bool u_xlatb19;
float u_xlat24;
int u_xlati24;
uint u_xlatu24;
float u_xlat25;
uint u_xlatu25;
float u_xlat27;
int u_xlati27;
bool u_xlatb27;
float u_xlat28;
uint u_xlatu28;
float u_xlat29;
float u_xlat30;
int u_xlati30;
void main()
{
	ImmCB_0_0_0[0] = vec4(1.0, 0.0, 0.0, 0.0);
	ImmCB_0_0_0[1] = vec4(0.0, 1.0, 0.0, 0.0);
	ImmCB_0_0_0[2] = vec4(0.0, 0.0, 1.0, 0.0);
	ImmCB_0_0_0[3] = vec4(0.0, 0.0, 0.0, 1.0);
    u_xlat0.xyz = _TimeParameters.yyy * Color_6DC7EDD5.xyz;
    u_xlat1.xy = vs_TEXCOORD3.yx + vec2(-0.5, -0.5);
    u_xlat24 = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat0.w = sqrt(u_xlat24);
    u_xlat0 = u_xlat0 * vec4(0.959999979, 0.959999979, 0.959999979, 5.0);
    u_xlat2.x = sin(u_xlat0.w);
    u_xlat3.x = cos(u_xlat0.w);
    u_xlat17.xy = u_xlat1.xy * u_xlat2.xx;
    u_xlat24 = u_xlat3.x * u_xlat1.y + (-u_xlat17.x);
    u_xlat1.x = u_xlat3.x * u_xlat1.x + u_xlat17.y;
    u_xlat24 = u_xlat24 + 0.5;
    u_xlat2.x = _TimeParameters.x * 0.0500000007 + u_xlat24;
    u_xlat24 = u_xlat1.x + 0.5;
    u_xlat2.y = _TimeParameters.x * 0.0500000007 + u_xlat24;
    u_xlat1.xy = u_xlat2.xy * vec2(6.0, 6.0);
    u_xlat17.xy = floor(u_xlat1.xy);
    u_xlat1.xy = fract(u_xlat1.xy);
    u_xlat2.x = float(0.0);
    u_xlat2.y = float(8.0);
    for(int u_xlati_loop_1 = int(0xFFFFFFFFu) ; u_xlati_loop_1<=1 ; u_xlati_loop_1++)
    {
        u_xlat3.y = float(u_xlati_loop_1);
        u_xlat18.xy = u_xlat2.xy;
        for(int u_xlati_loop_2 = int(0xFFFFFFFFu) ; u_xlati_loop_2<=1 ; u_xlati_loop_2++)
        {
            u_xlat3.x = float(u_xlati_loop_2);
            u_xlat4.xy = u_xlat17.xy + u_xlat3.xy;
            u_xlat27 = dot(u_xlat4.xy, vec2(15.2700005, 99.4100037));
            u_xlat4.x = dot(u_xlat4.xy, vec2(47.6300011, 89.9800034));
            u_xlat5.x = sin(u_xlat27);
            u_xlat5.y = sin(u_xlat4.x);
            u_xlat4.xy = u_xlat5.xy * vec2(46839.3203, 46839.3203);
            u_xlat4.xy = fract(u_xlat4.xy);
            u_xlat27 = sin(u_xlat4.y);
            u_xlat5.x = u_xlat27 * 0.5 + 0.5;
            u_xlat27 = cos(u_xlat4.x);
            u_xlat5.y = u_xlat27 * 0.5 + 0.5;
            u_xlat3.xw = u_xlat3.xy + u_xlat5.xy;
            u_xlat3.xw = (-u_xlat1.xy) + u_xlat3.xw;
            u_xlat3.x = dot(u_xlat3.xw, u_xlat3.xw);
            u_xlat3.x = sqrt(u_xlat3.x);
#ifdef UNITY_ADRENO_ES3
            u_xlatb27 = !!(u_xlat3.x<u_xlat18.y);
#else
            u_xlatb27 = u_xlat3.x<u_xlat18.y;
#endif
            u_xlat18.xy = (bool(u_xlatb27)) ? u_xlat3.xx : u_xlat18.xy;
        }
        u_xlat2.xy = u_xlat18.xy;
    }
    u_xlat24 = dot(vs_TEXCOORD1.xyz, vs_TEXCOORD1.xyz);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat1.xyz = vec3(u_xlat24) * vs_TEXCOORD1.xyz;
    u_xlat24 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat24 = max(u_xlat24, 1.17549435e-38);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat10.xyz = vec3(u_xlat24) * vs_TEXCOORD4.xyz;
    u_xlat25 = unity_LightData.z * unity_ProbesOcclusion.x;
    vec3 txVec0 = vec3(vs_TEXCOORD9.xy,vs_TEXCOORD9.z);
    u_xlat16_3 = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec0, 0.0);
    u_xlat11.x = (-_MainLightShadowParams.x) + 1.0;
    u_xlat3.x = u_xlat16_3 * _MainLightShadowParams.x + u_xlat11.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(0.0>=vs_TEXCOORD9.z);
#else
    u_xlatb11 = 0.0>=vs_TEXCOORD9.z;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb19 = !!(vs_TEXCOORD9.z>=1.0);
#else
    u_xlatb19 = vs_TEXCOORD9.z>=1.0;
#endif
    u_xlatb11 = u_xlatb19 || u_xlatb11;
    u_xlat3.x = (u_xlatb11) ? 1.0 : u_xlat3.x;
    u_xlat11.x = dot((-u_xlat10.xyz), u_xlat1.xyz);
    u_xlat11.x = u_xlat11.x + u_xlat11.x;
    u_xlat11.xyz = u_xlat1.xyz * (-u_xlat11.xxx) + (-u_xlat10.xyz);
    u_xlat4.x = dot(u_xlat1.xyz, u_xlat10.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat4.x = min(max(u_xlat4.x, 0.0), 1.0);
#else
    u_xlat4.x = clamp(u_xlat4.x, 0.0, 1.0);
#endif
    u_xlat4.x = (-u_xlat4.x) + 1.0;
    u_xlat4.x = u_xlat4.x * u_xlat4.x;
    u_xlat4.x = u_xlat4.x * u_xlat4.x;
    u_xlat16_5 = textureLod(unity_SpecCube0, u_xlat11.xyz, 4.05000019);
    u_xlat16_11 = u_xlat16_5.w + -1.0;
    u_xlat11.x = unity_SpecCube0_HDR.w * u_xlat16_11 + 1.0;
    u_xlat11.x = max(u_xlat11.x, 0.0);
    u_xlat11.x = log2(u_xlat11.x);
    u_xlat11.x = u_xlat11.x * unity_SpecCube0_HDR.y;
    u_xlat11.x = exp2(u_xlat11.x);
    u_xlat11.x = u_xlat11.x * unity_SpecCube0_HDR.x;
    u_xlat11.xyz = u_xlat16_5.xyz * u_xlat11.xxx;
    u_xlat11.xyz = u_xlat11.xyz * vec3(0.941176474, 0.941176474, 0.941176474);
    u_xlat4.x = u_xlat4.x * 0.5 + 0.0399999991;
    u_xlat11.xyz = u_xlat11.xyz * u_xlat4.xxx;
    u_xlat11.xyz = vs_TEXCOORD7.xyz * u_xlat0.xyz + u_xlat11.xyz;
    u_xlat25 = u_xlat25 * u_xlat3.x;
    u_xlat3.x = dot(u_xlat1.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat25 = u_xlat25 * u_xlat3.x;
    u_xlat4.xyz = vec3(u_xlat25) * _MainLightColor.xyz;
    u_xlat5.xyz = vs_TEXCOORD4.xyz * vec3(u_xlat24) + _MainLightPosition.xyz;
    u_xlat24 = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlat24 = max(u_xlat24, 1.17549435e-38);
    u_xlat24 = inversesqrt(u_xlat24);
    u_xlat5.xyz = vec3(u_xlat24) * u_xlat5.xyz;
    u_xlat24 = dot(u_xlat1.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat24 = min(max(u_xlat24, 0.0), 1.0);
#else
    u_xlat24 = clamp(u_xlat24, 0.0, 1.0);
#endif
    u_xlat25 = dot(_MainLightPosition.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat25 = min(max(u_xlat25, 0.0), 1.0);
#else
    u_xlat25 = clamp(u_xlat25, 0.0, 1.0);
#endif
    u_xlat24 = u_xlat24 * u_xlat24;
    u_xlat24 = u_xlat24 * -0.9375 + 1.00001001;
    u_xlat25 = u_xlat25 * u_xlat25;
    u_xlat24 = u_xlat24 * u_xlat24;
    u_xlat25 = max(u_xlat25, 0.100000001);
    u_xlat24 = u_xlat24 * u_xlat25;
    u_xlat24 = u_xlat24 * 3.0;
    u_xlat24 = 0.0625 / u_xlat24;
    u_xlat5.xyz = vec3(u_xlat24) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat0.xyz;
    u_xlat3.xyz = u_xlat5.xyz * u_xlat4.xyz + u_xlat11.xyz;
    u_xlat24 = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlatu24 = uint(int(u_xlat24));
    u_xlat4.xyz = u_xlat3.xyz;
    for(uint u_xlatu_loop_3 = uint(0u) ; u_xlatu_loop_3<u_xlatu24 ; u_xlatu_loop_3++)
    {
        u_xlati27 = int(uint(u_xlatu_loop_3 & 3u));
        u_xlatu28 = uint(u_xlatu_loop_3 >> 2u);
        u_xlat27 = dot(unity_LightIndices[int(u_xlatu28)], ImmCB_0_0_0[u_xlati27]);
        u_xlati27 = int(u_xlat27);
        u_xlat5.xyz = (-vs_TEXCOORD0.xyz) * _AdditionalLightsPosition[u_xlati27].www + _AdditionalLightsPosition[u_xlati27].xyz;
        u_xlat28 = dot(u_xlat5.xyz, u_xlat5.xyz);
        u_xlat28 = max(u_xlat28, 6.10351563e-05);
        u_xlat29 = inversesqrt(u_xlat28);
        u_xlat6.xyz = vec3(u_xlat29) * u_xlat5.xyz;
        u_xlat30 = float(1.0) / float(u_xlat28);
        u_xlat28 = u_xlat28 * _AdditionalLightsAttenuation[u_xlati27].x;
        u_xlat28 = (-u_xlat28) * u_xlat28 + 1.0;
        u_xlat28 = max(u_xlat28, 0.0);
        u_xlat28 = u_xlat28 * u_xlat28;
        u_xlat28 = u_xlat28 * u_xlat30;
        u_xlat30 = dot(_AdditionalLightsSpotDir[u_xlati27].xyz, u_xlat6.xyz);
        u_xlat30 = u_xlat30 * _AdditionalLightsAttenuation[u_xlati27].z + _AdditionalLightsAttenuation[u_xlati27].w;
#ifdef UNITY_ADRENO_ES3
        u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
        u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
        u_xlat30 = u_xlat30 * u_xlat30;
        u_xlat28 = u_xlat28 * u_xlat30;
        u_xlati30 = int(_AdditionalLightsOcclusionProbes[u_xlati27].x);
        u_xlat30 = dot(unity_ProbesOcclusion, ImmCB_0_0_0[u_xlati30]);
        u_xlat30 = max(u_xlat30, _AdditionalLightsOcclusionProbes[u_xlati27].y);
        u_xlat28 = u_xlat28 * u_xlat30;
        u_xlat30 = dot(u_xlat1.xyz, u_xlat6.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat30 = min(max(u_xlat30, 0.0), 1.0);
#else
        u_xlat30 = clamp(u_xlat30, 0.0, 1.0);
#endif
        u_xlat28 = u_xlat28 * u_xlat30;
        u_xlat7.xyz = vec3(u_xlat28) * _AdditionalLightsColor[u_xlati27].xyz;
        u_xlat5.xyz = u_xlat5.xyz * vec3(u_xlat29) + u_xlat10.xyz;
        u_xlat27 = dot(u_xlat5.xyz, u_xlat5.xyz);
        u_xlat27 = max(u_xlat27, 1.17549435e-38);
        u_xlat27 = inversesqrt(u_xlat27);
        u_xlat5.xyz = vec3(u_xlat27) * u_xlat5.xyz;
        u_xlat27 = dot(u_xlat1.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat27 = min(max(u_xlat27, 0.0), 1.0);
#else
        u_xlat27 = clamp(u_xlat27, 0.0, 1.0);
#endif
        u_xlat28 = dot(u_xlat6.xyz, u_xlat5.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat28 = min(max(u_xlat28, 0.0), 1.0);
#else
        u_xlat28 = clamp(u_xlat28, 0.0, 1.0);
#endif
        u_xlat27 = u_xlat27 * u_xlat27;
        u_xlat27 = u_xlat27 * -0.9375 + 1.00001001;
        u_xlat28 = u_xlat28 * u_xlat28;
        u_xlat27 = u_xlat27 * u_xlat27;
        u_xlat28 = max(u_xlat28, 0.100000001);
        u_xlat27 = u_xlat27 * u_xlat28;
        u_xlat27 = u_xlat27 * 3.0;
        u_xlat27 = 0.0625 / u_xlat27;
        u_xlat5.xyz = vec3(u_xlat27) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat0.xyz;
        u_xlat4.xyz = u_xlat5.xyz * u_xlat7.xyz + u_xlat4.xyz;
    }
    SV_TARGET0.xyz = u_xlat2.xxx + u_xlat4.xyz;
    SV_TARGET0.w = 1.0;
    return;
}

#endif
                             $GlobalsP
  
      _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                     0      _AdditionalLightsColor                    0     _AdditionalLightsAttenuation                  0     _AdditionalLightsSpotDir                  0      _AdditionalLightsOcclusionProbes                  0     _TimeParameters                   0
     _MainLightShadowParams                    @
         UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          UnityPerMaterial         Color_6DC7EDD5                               $Globals�        _WorldSpaceCameraPos                         unity_MatrixVP                         _MainLightWorldToShadow                 P             unity_SpecCube0                   _MainLightShadowmapTexture                  UnityPerDraw              UnityPerMaterial          